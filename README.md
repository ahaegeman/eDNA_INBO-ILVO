# eDNA pipeline INBO/ILVO

## Introduction
This pipeline is a customized pipeline built to analyse amplicon sequencing data resulting from the library preparation method described below.

### Library preparation
The library preparation method consists of a PCR step with custom primers to amplify the region of interest. In this case we mainly use two primer sets, called Riaz and Teleo.

| Primerset      | Forward primer     | Reverse primer       | Reference                                                  |
|----------------|--------------------|----------------------|------------------------------------------------------------|
| Riaz           | ACTGGGATTAGATACCCC | TAGAACAGGCTCCTCTAG   | [Riaz et al, 2011](https://doi.org/10.1093/nar/gkr732)     |
| Teleo          | ACACCGCCCGTCACTCT  | CTTCCGGTACACTTACCRTG | [Valentini et al, 2016](https://doi.org/10.1111/mec.13428) |

The actual primers used in the PCR also contain specific indices that can later be used in the data analysis to discriminate the samples. These indices are used in pairs and are never reused; i.e. each index is used in one combination only, and each combination corresponds to a specific sample.
Below you can see a few examples of primer pairs used for specific samples. Note that the indices can have different lengths.

| Sample | Forward primer                | Reverse primer                     |
|--------|-------------------------------|------------------------------------|
| 1      | **AACAA**ACACCGCCCGTCACTCT    | **GGTCGGTGTA**CTTCCGGTACACTTACCRTG |
| 2      | **TGCATGT**ACACCGCCCGTCACTCT  | **CACCAGCTCC**CTTCCGGTACACTTACCRTG |
| 3      | **ATCCTGCT**ACACCGCCCGTCACTCT | **CCTCGCTAC**CTTCCGGTACACTTACCRTG  |


After the PCR and cleanup, Illumina adapters are ligated to the amplicons. These adapters can again contain custom indices for an extra level of indexing, making the number of samples that can be pooled in one sequencing lane even larger.

The advantages of this library prep method are:
- Only 1 PCR instead of a two-step PCR protocol (less PCR bias)
- Relatively short primers due to extra ligation step to ligate Illumina adapters (compared to 1-step PCR protocols)
- No phiX needs to be added to diversify the run because the fragments are ligated in both directions and the indices have different lengths. This leads to higher quality data and more reads.
- Index switching almost impossible since we only retain read pairs in which both indices are found without errors, and the indices are used in only one combination.
- The number of samples which can be pooled is in principle only limited by the number of primer sets you order; and can even be enlarged by making use of an extra index in the adapters of the ligation step.

The disadvantages of this library prep method are:
- A lot of different primers need to be ordered (because of the unique index combination per sample), which makes the start-up cost higher.
- Demultiplexing could not be done using existing tools (see further).

### Data analysis pipeline

The data analysis pipeline consists of three main steps:
1. Demultiplexing
2. Cleanup (quality filtering, primer removal)
3. Sequence variant counting

The pipeline uses primarily third party software (described below) called through shell scripts.

## Installation requirements

The following software should be installed on your system to run the scripts in this repository (older or newer versions of the software might work as well, but this was not tested). Make sure to check the license of each program to verify whether you are allowed to use it or not.

* Operating system: Ubuntu 18.04
* [sabre 1.000](https://github.com/najoshi/sabre) (demultiplexing tool, should be in $PATH)
* [cutadapt 1.15](https://cutadapt.readthedocs.io/en/stable/guide.html) (primer removal, should be in $PATH)
* [pear 0.9.11](https://cme.h-its.org/exelixis/web/software/pear/) (merging F and R reads, should be in $PATH)
* [vsearch 2.7.1](https://github.com/torognes/vsearch) (quality filtering, should be in $PATH)
* [OBITools 1.2.13](https://pythonhosted.org/OBITools/welcome.html) (sequence variant cleaning and counting, commands should be in $PATH)


## Demultiplexing script
For the demultiplexing step, at the time we started this project (2016), no suitable tools were identified which could handle indices of variable length AND handle the fact that we want to use a combination of indices to identify the sample AND that the fragments could be in two directions (the library is undirectional).
To achieve the demultiplexing, we use the software `sabre` which can handle variable length barcodes, but cannot handle the other issues (barcodes both in F and R reads and undirectional sequencing). The latter issues were covered by running `sabre` multiple times in multiple directions using a shell script.

The script is used as follows:
`eDNA_demultiplexing.sh -b barcodefile.txt -f forwardreads.fq -r reversereads.fq`

- `-b` : tab delimited file; column 1: name of sample corresponding to a certain  barcode combination; column2: barcode used in primer 1; column3 : barcode used in primer 2
- `-f` : forward reads file to be demultiplexed in fastq format (should be unzipped)
- `-r` : reverse reads file to be demultiplexed in fastq format (should be unzipped)

An example of a barcode file is shown below (tab delimited file).

|         	|         	|            	|
| ---------	| --------- | ----------- |
| Sample1 	| AACAA    	| GGTCGGTGTA 	|
| Sample2 	| TGCATGT  	| CACCAGCTCC 	|
| Sample3 	| ATCCTGCT 	| CCTCGCTAC  	|


## Pipeline
The analysis pipeline consists of a single shell script which does the following:
1. Merging of forward and reverse reads using [pear](https://cme.h-its.org/exelixis/web/software/pear/)
2. Removal of primers using [cutadapt](https://cutadapt.readthedocs.io/en/stable/guide.html)
3. Quality filtering using `fastq_filter`from [vsearch](https://github.com/torognes/vsearch), with a maximum expected error of 0.5
4. Renaming the sequences to contain the sample name and concatenating the sequences of all samples
5. Dereplicate the sequences using `obiuniq` from [OBITools](https://pythonhosted.org/OBITools/welcome.html)
6. Filtering and cleaning the sequences using `obigrep` and `obiclean` from [OBITools](https://pythonhosted.org/OBITools/welcome.html) with as settings `-p "count>=mincount"`, `-r 0.05`, `-H` and `-d 1`
7. Assigning a taxonomy to the sequences using `ecotag` from [OBITools](https://pythonhosted.org/OBITools/welcome.html)
8. Generating an output table using `obiannotate`, `obisort` and `obitab` from [OBITools](https://pythonhosted.org/OBITools/welcome.html)

The output of each step is stored in subdirectories which are made in the folder where the input data is.

The script is used as follows:

`bash eDNA_pipeline.sh -f fragment -d input_directory -r reference_database -a amplicon_database -m mincount`

- `-f` : fragment which is being analyzed, currently "Riaz" and "Teleo" are supported. These correspond to the primers as mentioned in the "Library preparation" section above.
- `-d` : directory which contains the demultiplexed sequence files, ending in `_1.fq` (forward reads) and `_2.fq` (reverse reads)
- `-r` : full path to the reference database in [ecoPCR](https://pythonhosted.org/OBITools/scripts/ecoPCR.html) format created by `obiconvert`
- `-a` : full path to the amplicon database in fasta format (result of [ecoPCR](https://pythonhosted.org/OBITools/scripts/ecoPCR.html) and [OBITools](https://pythonhosted.org/OBITools/welcome.html) commands `ecoPCR` and `obiuniq`)
- `-m` : minimum number of counts (summed over all samples) for a sequence to be kept

Example:

`bash eDNA_pipeline.sh -f Teleo -d /dir/data -r /dir/ecopcr_db -a /dir/ecopcr_amplified_clean_uniq.fasta -m 80`
